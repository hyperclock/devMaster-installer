devMaster-installer
==========================

Live-Installer fork from [Fervi's Remastersys Installer](https://github.com/fervi/live-installer-remastersys).

Reworked, modded and rebranded for the [Cybernux OS](http://www.cybernux.org) Project.

Use this tool along with [_devMaster_](https://gitlab.com/hyperclock/devMaster.git).


